(function ($, Drupal) {
  "use strict";

  Drupal.behaviors.zebraEDSLineChart = {
    attach: function (context, settings) {
      $('.zebra-eds-line-chart', context).once('zebraEDSLineChart').each(function () {
        var $this = $(this);

        $.ajax({
          url: $this.attr('data-src'),
          dataType: 'json',
          success: function (json) {
            $this.append('<svg class="zebra-eds-line-chart__chart"></svg>');
console.log(json)
            // Prepare all necessary data and constants for our visualizer.
            var vis = d3.select($this.find('svg')[0]),
                WIDTH = 700,
                HEIGHT = 500,
                MARGINS = {
                  top: 20,
                  right: 20,
                  bottom: 20,
                  left: 50
                },
                minDate = new Date(json[0].date),
                maxDate = new Date(json[json.length-1].date),
                xScale = d3.time.scale()
                  .range([MARGINS.left, WIDTH - MARGINS.right])
                  // The x axis contains the date data.
                  .domain([minDate, maxDate]),
                yScale = d3.scale.linear()
                  .range([HEIGHT - MARGINS.top, MARGINS.bottom])
                  // The y axis contains the pain and moral data, which go from
                  // 0 to 10.
                  .domain([0, 10]),
                xAxis = d3.svg.axis()
                  .scale(xScale),
                yAxis = d3.svg.axis()
                  .scale(yScale)
                  .orient('left'),
                painLineGen = d3.svg.line()
                  .x(function(d) {
                    return xScale(new Date(d.date));
                  })
                  .y(function(d) {
                    return yScale(d.pain);
                  }),
                moralLineGen = d3.svg.line()
                  .x(function(d) {
                    return xScale(new Date(d.date));
                  })
                  .y(function(d) {
                    return yScale(d.moral);
                  });

            vis.append('svg:g')
              .attr('transform', 'translate(0, ' + (HEIGHT - MARGINS.bottom) + ')')
              .call(xAxis);

            vis.append('svg:g')
              .attr('transform', 'translate(' + (MARGINS.left) + ',0)')
              .call(yAxis);

            vis.append('svg:path')
              .attr('d', painLineGen(json))
              .attr('stroke', 'red')
              .attr('stroke-width', 1)
              .attr('fill', 'none');
            vis.append('svg:path')
              .attr('d', moralLineGen(json))
              .attr('stroke', 'green')
              .attr('stroke-width', 1)
              .attr('fill', 'none');
          }
        });
      });
    }
  };

})(jQuery, Drupal);
